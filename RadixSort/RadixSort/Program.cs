﻿

namespace modul2;

class Program
{
    static void Sort(int[] arr)
    {
        int i, j;
        int[] tmp = new int[arr.Length];
        for (int shift = 31; shift >= 0; --shift)
        {
            j = 0;
            for (i = 0; i < arr.Length; ++i)
            {
                bool move = (arr[i] << shift) >= 0;
                if (shift == 0 ? !move : move)
                {
                    arr[i - j] = arr[i];
                }    
                    
                else
                {
                    tmp[j++] = arr[i];
                }
                    
            }
            Array.Copy(tmp, 0, arr, arr.Length - j, j);
        }
    }
        
    static void Main(string[] args)
    {
        int size = 20;

        Random rand = new Random();
        int[] arr = new int[size];

        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = rand.Next(500);
        }

        Console.WriteLine("\nOriginal array : ");
        foreach (var item in arr)
        {
            Console.Write(" " + item);
        }

        Sort(arr);

        Console.WriteLine("\nSorted array: ");
        foreach (var item in arr)
        {
            Console.Write(" " + item);
        }
    }
}